package com.kp.keniamusikproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.format.Time;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.kp.keniamusikproject.bs.BibleStudy;
import com.kp.keniamusikproject.countdown_timer.Logger;
import com.kp.keniamusikproject.progressbar.ProgressWheel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "CountdownTimer";

    private TextView mCountdownNote;
    private ProgressWheel mDaysWheel;
    private TextView mDaysLabel;
    private ProgressWheel mHoursWheel;
    private TextView mHoursLabel;
    private ProgressWheel mMinutesWheel;
    private TextView mMinutesLabel;
    private ProgressWheel mSecondsWheel;
    private TextView mSecondsLabel;
    private TextView footerText;

    // Timer setup
    Time conferenceTime = new Time(Time.getCurrentTimezone());
    int hour = 19;
    int minute = 00;
    int second = 0;
    int monthDay = 30;
    // month is zero based...7 == August
    int month = 7;
    int year;

    // Values displayed by the timer
    private int mDisplayDays;
    private int mDisplayHours;
    private int mDisplayMinutes;
    private int mDisplaySeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        configureViews();
        configureConferenceDate();
    }

    private void configureViews() {

        // Font path
        String fontPath5 = "fonts/barbaro.ttf";


        this.conferenceTime.setToNow();
        this.year = conferenceTime.year;

        this.mCountdownNote = (TextView) findViewById(R.id.activity_countdown_timer_note);
        this.mDaysWheel = (ProgressWheel) findViewById(R.id.activity_countdown_timer_days);
        this.mHoursWheel = (ProgressWheel) findViewById(R.id.activity_countdown_timer_hours);
        this.mMinutesWheel = (ProgressWheel) findViewById(R.id.activity_countdown_timer_minutes);
        this.mSecondsWheel = (ProgressWheel) findViewById(R.id.activity_countdown_timer_seconds);
        this.mDaysLabel = (TextView) findViewById(R.id.activity_countdown_timer_days_text);
        this.mHoursLabel = (TextView) findViewById(R.id.activity_countdown_timer_hours_text);
        this.mMinutesLabel = (TextView) findViewById(R.id.activity_countdown_timer_minutes_text);
        this.mSecondsLabel = (TextView) findViewById(R.id.activity_countdown_timer_seconds_text);
        this.footerText = (TextView) findViewById(R.id.footerText);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath5);
        Typeface days = Typeface.createFromAsset(getAssets(), fontPath5);
        Typeface hrs = Typeface.createFromAsset(getAssets(), fontPath5);
        Typeface min = Typeface.createFromAsset(getAssets(), fontPath5);
        Typeface sec = Typeface.createFromAsset(getAssets(), fontPath5);
        Typeface ft = Typeface.createFromAsset(getAssets(), fontPath5);

        // Applying font
        mCountdownNote.setTypeface(tf);
        mDaysLabel.setTypeface(days);
        mHoursLabel.setTypeface(hrs);
        mMinutesLabel.setTypeface(min);
        mSecondsLabel.setTypeface(sec);
        footerText.setTypeface(ft);

    }

    private void configureConferenceDate() {
        conferenceTime.set(second, minute, hour, monthDay, month, year);
        conferenceTime.normalize(true);
        long confMillis = conferenceTime.toMillis(true);

        Time nowTime = new Time(Time.getCurrentTimezone());
        nowTime.setToNow();
        nowTime.normalize(true);
        long nowMillis = nowTime.toMillis(true);

        long milliDiff = confMillis - nowMillis;

        new CountDownTimer(milliDiff, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                // decompose difference into days, hours, minutes and seconds
                MainActivity.this.mDisplayDays = (int) ((millisUntilFinished / 1000) / 86400);
                MainActivity.this.mDisplayHours = (int) (((millisUntilFinished / 1000) - (MainActivity.this.mDisplayDays * 86400)) / 3600);
                MainActivity.this.mDisplayMinutes = (int) (((millisUntilFinished / 1000) - ((MainActivity.this.mDisplayDays * 86400) + (MainActivity.this.mDisplayHours * 3600))) / 60);
                MainActivity.this.mDisplaySeconds = (int) ((millisUntilFinished / 1000) % 60);

                MainActivity.this.mDaysWheel.setText(String.valueOf(MainActivity.this.mDisplayDays));
                MainActivity.this.mDaysWheel.setProgress(MainActivity.this.mDisplayDays);

                MainActivity.this.mHoursWheel.setText(String.valueOf(MainActivity.this.mDisplayHours));
                MainActivity.this.mHoursWheel.setProgress(MainActivity.this.mDisplayHours * 15);

                MainActivity.this.mMinutesWheel.setText(String.valueOf(MainActivity.this.mDisplayMinutes));
                MainActivity.this.mMinutesWheel.setProgress(MainActivity.this.mDisplayMinutes * 6);

                Animation an = new RotateAnimation(0.0f, 90.0f, 250f, 273f);
                an.setFillAfter(true);

                MainActivity.this.mSecondsWheel.setText(String.valueOf(MainActivity.this.mDisplaySeconds));
                MainActivity.this.mSecondsWheel.setProgress(MainActivity.this.mDisplaySeconds * 6);
            }

            @Override
            public void onFinish() {
                //TODO: this is where you would launch a subsequent activity if you'd like.  I'm currently just setting the seconds to zero
                Logger.d(TAG, "Timer Finished...");
                MainActivity.this.mSecondsWheel.setText("0");
                MainActivity.this.mSecondsWheel.setProgress(0);
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Kenia Musik Projekt App");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Im using the Kenia Musik Projekt Application. Get it from https://play.google.com/store/apps/details?id=com.kenyaweb.kenyawebapp  #AtawaleTour");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calender) {
            // Handle the camera action
            Intent a = new Intent(MainActivity.this, Tour_Calendar.class);
            startActivity(a);
        } else if (id == R.id.nav_map) {
            Intent a = new Intent(MainActivity.this, LiteListDemoActivity.class);
            startActivity(a);

        } else if (id == R.id.nav_about) {
            Intent a = new Intent(MainActivity.this, AboutKP.class);
            startActivity(a);

        } else if (id == R.id.nav_devotion) {
            Intent a = new Intent(MainActivity.this, BibleStudy.class);
            startActivity(a);

        } else if (id == R.id.nav_contact_us) {
            Intent a = new Intent(MainActivity.this, ContactUs.class);
            startActivity(a);

        } else if (id == R.id.nav_support) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
