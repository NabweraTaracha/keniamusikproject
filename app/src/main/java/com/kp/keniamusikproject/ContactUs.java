package com.kp.keniamusikproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by nabwera_taracha on 7/29/16.
 */
public class ContactUs extends AppCompatActivity {

    Toolbar toolbar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.contact_us);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        findViewById(R.id.btn_call_ke).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_mail_ke).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_call_de).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_mail_de).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_website).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_fb).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_tw).setOnClickListener(mGlobal_OnClickListener);
        findViewById(R.id.btn_ig).setOnClickListener(mGlobal_OnClickListener);





    }

    //Global On click listener for all views
    final View.OnClickListener mGlobal_OnClickListener = new View.OnClickListener() {
        public void onClick(final View v) {
            switch(v.getId()) {
                case R.id.btn_call_ke:
                    //Inform the user the button1 has been clicked
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:+254728820434"));
                    startActivity(intent);;
                    break;


                case R.id.btn_mail_ke:
                    String aEmailList[] = { "kp@mamlakahillchapel.org"};
                    String CCList[] = { "samuelithiga@mamlakahillchapel.org"};

                    Intent intent_mail_nrb = new Intent(Intent.ACTION_SENDTO);
                    intent_mail_nrb.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent_mail_nrb.putExtra(Intent.EXTRA_EMAIL, aEmailList);
                    intent_mail_nrb.putExtra(Intent.EXTRA_CC, CCList);
                    intent_mail_nrb.putExtra(Intent.EXTRA_SUBJECT, "About KP");
                    startActivity(intent_mail_nrb);
                    break;

                case R.id.btn_call_de:
                    //Inform the user the button1 has been clicked
                    Intent call_de = new Intent(Intent.ACTION_DIAL);
                    call_de.setData(Uri.parse("tel:033432/687"));
                    startActivity(call_de);;
                    break;

                case R.id.btn_mail_de:
                    String deEmailList[] = { "vorstand@kenia-musikprojekt.de"};
                    String deCCList[] = { "leitungsteam@kenia-musikprojekt.de"};

                    Intent intent_mail_de = new Intent(Intent.ACTION_SENDTO);
                    intent_mail_de.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent_mail_de.putExtra(Intent.EXTRA_EMAIL, deEmailList);
                    intent_mail_de.putExtra(Intent.EXTRA_CC, deCCList);
                    intent_mail_de.putExtra(Intent.EXTRA_SUBJECT, "About KP");
                    startActivity(intent_mail_de);
                    break;

                case R.id.btn_website:;
                    Intent intent_web = new Intent(ContactUs.this, Website.class);
                    startActivity(intent_web);
                    break;

                case R.id.btn_fb:
                    Intent fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/13252590786/"));
                    startActivity(fbIntent);
                    break;

                case R.id.btn_tw:
                    Intent twIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/MamlakaChapel"));
                    startActivity(twIntent);
                    break;

                case R.id.btn_ig:
                    Intent igIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/kenyamusicproject/"));
                    startActivity(igIntent);
                    break;

            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    http://kenia-musikprojekt.de/
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.contact_us, container, false);
//        btn_call_ke=(Button) view.findViewById(R.id.btn_call_ke);
//        btn_call_ke.setOnClickListener(this);
//
//        btn_call_de = (Button) view.findViewById(R.id.btn_call_de);
//        btn_call_de.setOnClickListener(this);
//
//        btn_mail_ke = (Button) view.findViewById(R.id.btn_mail_ke);
//        btn_mail_ke.setOnClickListener(this);
//
//        btn_mail_de = (Button) view.findViewById(R.id.btn_mail_de);
//        btn_mail_de.setOnClickListener(this);
//
//        btn_website = (Button) view.findViewById(R.id.btn_website);
//        btn_website.setOnClickListener(this);
//
//        btn_fb = (Button) view.findViewById(R.id.btn_fb);
//        btn_fb.setOnClickListener(this);
//
//        btn_tw = (Button) view.findViewById(R.id.btn_tw);
//        btn_tw.setOnClickListener(this);
//
//        btn_ig = (Button) view.findViewById(R.id.btn_ig);
//        btn_ig.setOnClickListener(this);
//
//        return view;
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.btn_call_ke:
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:0722205594"));
//                startActivity(intent);
//                break;
//
//            case R.id.btn_call_de:
//                Intent intent_msa = new Intent(Intent.ACTION_DIAL);
//                intent_msa.setData(Uri.parse("tel:0412229722"));
//                startActivity(intent_msa);
//                break;
//
//            case R.id.btn_mail_ke:
//                String aEmailList[] = { "info@kenyaweb.com"};
//                String CCList[] = { "marketing@kenyaweb.com"};
//
//                Intent intent_mail_nrb = new Intent(Intent.ACTION_SENDTO);
//                intent_mail_nrb.setData(Uri.parse("mailto:")); // only email apps should handle this
//                intent_mail_nrb.putExtra(Intent.EXTRA_EMAIL, aEmailList);
//                intent_mail_nrb.putExtra(Intent.EXTRA_CC, CCList);
//                intent_mail_nrb.putExtra(Intent.EXTRA_SUBJECT, "Service Inquiry");
//                startActivity(intent_mail_nrb);
//                break;
//
//            case R.id.btn_mail_de:
//                String emailList[] = { "salesmsa@kenyaweb.com"};
//                String ccList[] = { "marketing@kenyaweb.com"};
//
//                Intent intent_mail_msa = new Intent(Intent.ACTION_SENDTO);
//                intent_mail_msa.setData(Uri.parse("mailto:")); // only email apps should handle this
//                intent_mail_msa.putExtra(Intent.EXTRA_EMAIL, emailList);
//                intent_mail_msa.putExtra(Intent.EXTRA_CC, ccList);
//                intent_mail_msa.putExtra(Intent.EXTRA_SUBJECT, "Service Inquiry");
//                startActivity(intent_mail_msa);
//                break;
//
//            case R.id.btn_website:;
//                Intent intent_web = new Intent(ContactUs.this, AboutKP.class);
//                startActivity(intent_web);
//                break;
//
//            case R.id.btn_fb:
//                Intent fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/kenyaweblimited/?fref=nf"));
//                startActivity(fbIntent);
//                break;
//
//            case R.id.btn_tw:
//                Intent twIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/KenyaweBdotCom"));
//                startActivity(twIntent);
//                break;
//
//            case R.id.btn_ig:
//                Intent lnIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/kenyaweb-limited-453b6a120"));
//                startActivity(lnIntent);
//                break;
//
//        }
    }


