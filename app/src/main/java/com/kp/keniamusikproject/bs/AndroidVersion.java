package com.kp.keniamusikproject.bs;

/**
 * Created by nabwera_taracha on 7/28/16.
 */
public class AndroidVersion {

    private String recyclerViewTitleText;
    private int recyclerViewImage;

    public String getrecyclerViewTitleText() {
        return recyclerViewTitleText;
    }

    public void setAndroidVersionName(String recyclerVietTitleText) {
        this.recyclerViewTitleText = recyclerVietTitleText;
    }

    public int getrecyclerViewImage() {
        return recyclerViewImage;
    }

    public void setrecyclerViewImage(int recyclerViewImage) {
        this.recyclerViewImage = recyclerViewImage;
    }
}
