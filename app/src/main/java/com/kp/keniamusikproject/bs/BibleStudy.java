package com.kp.keniamusikproject.bs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kp.keniamusikproject.R;
import com.kp.keniamusikproject.bs.days.Bonus;
import com.kp.keniamusikproject.bs.days.Friday;
import com.kp.keniamusikproject.bs.days.Monday;
import com.kp.keniamusikproject.bs.days.Saturday;
import com.kp.keniamusikproject.bs.days.Sunday;
import com.kp.keniamusikproject.bs.days.Thursday;
import com.kp.keniamusikproject.bs.days.Tuesday;
import com.kp.keniamusikproject.bs.days.Wednesday;

import java.util.ArrayList;

/**
 * Created by nabwera_taracha on 7/28/16.
 */
public class BibleStudy extends AppCompatActivity {

    Toolbar toolbar = null;

    private final String recyclerViewTitleText[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Bonus"
    };

    private final int recyclerViewImages[] = {
            R.drawable.kpplaceholder, R.drawable.kpplaceholder, R.drawable.kpplaceholder2, R.drawable.kpplaceholder2, R.drawable.kpplaceholder, R.drawable.kpplaceholder,
            R.drawable.kpplaceholder2, R.drawable.kpplaceholder2

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bible_study);
        initRecyclerViews();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initRecyclerViews() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<AndroidVersion> av = prepareData();
        AndroidDataAdapter mAdapter = new AndroidDataAdapter(getApplicationContext(), av);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        switch (i) {
                            case 0:
                                Intent mon = new Intent(BibleStudy.this, Monday.class);
                                startActivity(mon);
                                Toast.makeText(view.getContext(), "Monday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 1:
                                Intent tue = new Intent(BibleStudy.this, Tuesday.class);
                                startActivity(tue);
                                Toast.makeText(view.getContext(), "Tuesday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 2:
                                Intent wed = new Intent(BibleStudy.this, Wednesday.class);
                                startActivity(wed);
                                Toast.makeText(view.getContext(), "Wednesday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 3:
                                Intent thur = new Intent(BibleStudy.this, Thursday.class);
                                startActivity(thur);
                                Toast.makeText(view.getContext(), "Thursday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 4:
                                Intent fri = new Intent(BibleStudy.this, Friday.class);
                                startActivity(fri);
                                Toast.makeText(view.getContext(), "Friday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 5:
                                Intent sat = new Intent(BibleStudy.this, Saturday.class);
                                startActivity(sat);
                                Toast.makeText(view.getContext(), "Saturday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 6:
                                Intent sun = new Intent(BibleStudy.this, Sunday.class);
                                startActivity(sun);
                                Toast.makeText(view.getContext(), "Sunday Devotion" , Toast.LENGTH_SHORT).show();
                                break;

                            case 7:
                                Intent bon = new Intent(BibleStudy.this, Bonus.class);
                                startActivity(bon);
                                Toast.makeText(view.getContext(), "Bonus" , Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                })
        );

    }

    private ArrayList<AndroidVersion> prepareData() {

        ArrayList<AndroidVersion> av = new ArrayList<>();
        for (int i = 0; i < recyclerViewTitleText.length; i++) {
            AndroidVersion mAndroidVersion = new AndroidVersion();
            mAndroidVersion.setAndroidVersionName(recyclerViewTitleText[i]);
            mAndroidVersion.setrecyclerViewImage(recyclerViewImages[i]);
            av.add(mAndroidVersion);
        }
        return av;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
